package com.festival.festivalnewsalarm

import com.festival.collector.FestivalDataCollector
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FestivalNewsAlarmApplication

fun main(args: Array<String>) {
    runApplication<FestivalNewsAlarmApplication>(*args)
    val foo = FestivalDataCollector()
    foo.getFestivalNews()
}
