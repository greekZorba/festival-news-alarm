package com.festival.collector

import com.festival.model.dto.FestivalSearchDto
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient

class FestivalDataCollector {


    fun getFestivalNews() {
        val client = WebClient.create("https://korean.visitkorea.or.kr")
        val response = client.post()
            .uri { uriBuilder ->
                val festivalSearchDto = FestivalSearchDto()

                uriBuilder.path("/call")
                    .queryParam("cmd", festivalSearchDto.cmd)
                    .queryParam("year", festivalSearchDto.year)
                    .queryParam("month", festivalSearchDto.month)
                    .queryParam("areaCode", festivalSearchDto.areaCode)
                    .queryParam("sigunguCode", festivalSearchDto.sigunguCode)
                    .queryParam("tagId", festivalSearchDto.tagId)
                    .queryParam("locationx", festivalSearchDto.locationx)
                    .queryParam("locationy", festivalSearchDto.locationy)
                    .queryParam("sortkind", festivalSearchDto.sortkind)
                    .queryParam("page", festivalSearchDto.page)
                    .queryParam("cnt", festivalSearchDto.cnt)
                    .build()
            }
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .flatMap { response ->
                response.bodyToMono(HashMap::class.java)
            }
            .subscribe { responseMap ->
                println("responseMap : ${responseMap["body"]}")
            }


    }

}
