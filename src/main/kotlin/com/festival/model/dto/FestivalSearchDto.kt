package com.festival.model.dto

data class FestivalSearchDto(
    val cmd: String = "FESTIVAL_CONTENT_LIST_VIEW",
    val year: String = "All",
    val month: String = "All",
    val areaCode: String = "All",
    val sigunguCode: String = "All",
    val tagId: String = "All",
    val locationx: String = "0",
    val locationy: String = "0",
    val sortkind: String = "1",
    val page: String = "1",
    val cnt: String = "10"
)